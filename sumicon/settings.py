"""
Django settings for sumicon project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '(#*9nqew^ftzd_qbbl7=k0!g8)d5q4n)u@kwt11ixezg14kgj%'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.humanize',

    # Custom apps
    'south',
    'import_export',
    'registration',
    'catalogo',
    # 'paypal.standard.ipn',
)

SITE_ID = 1

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'sumicon.urls'

WSGI_APPLICATION = 'sumicon.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-mx'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
#STATIC_ROOT = "staticfiles"
STATICFILES_DIRS = (
    "static",
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

# Cloudinary configuration
CLOUDINARY = {
    "cloud_name": "nodlr",
    "api_key": "293215663182885",
    "api_secret": "-IMWuHK80gZGzEfuA8zRsNMQVxk",
}

# Debug toolbar
DEBUG_TOOLBAR_PATCH_SETTINGS = False

# Django registration
ACCOUNT_ACTIVATION_DAYS = 7
LOGIN_REDIRECT_URL = "/compra"

# Email credentials configuration
EMAIL_USE_TLS = True
EMAIL_HOST = "smtp.gmail.com"
EMAIL_PORT = 587
EMAIL_HOST_USER = "aadelgado@fevaq.net"
EMAIL_HOST_PASSWORD = "19noviembre2008"

# Paypal settings
# PAYPAL_RECEIVER_EMAIL = "o0.aalvaaro.0o@gmail.com"
# PAYPAL_TEST = True

