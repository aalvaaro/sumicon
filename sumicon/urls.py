from django.conf import settings
from django.conf.urls import patterns, include, url
from  django.views.generic import TemplateView

from catalogo.views import CatalogoList, CatalogoDetail

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # url(r'^compra/paypal/', include('paypal.standard.ipn.urls')),
    (r'^accounts/', include('registration.backends.default.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'catalogo.views.home', name='home'),
    url(r'^somos/$', TemplateView.as_view(template_name='somos.html'), name='somos'),
    url(r'^contacto/$', TemplateView.as_view(template_name='contacto.html'),
        name='contacto'),
    url(r'^productos/$', CatalogoList.as_view(), name='productos'),
    url(r'^productos/(?P<categoria>\D+)/$', 'catalogo.views.categoria', name='categoria'),
    url(r'^producto/(?P<pk>\d+)/$', CatalogoDetail.as_view(), name='producto'),
    # url(r'^compra/$', 'catalogo.views.payment', name="compra"),
)


