from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import Producto

class ProductoResource(resources.ModelResource):

    class Meta:
        model = Producto
        exclude = ("id", "imagen", "ficha_tecnica", "codigo_barras", "descripcion", "slug")

class ProductoAdmin(ImportExportModelAdmin):
    resource_class = ProductoResource
    exclude = ("codigo_barras",)
    list_filter = ("nombre",)
    search_fields = ["nombre"]

    class Media:
        js = ("js/tiny_mce/tiny_mce/tiny_mce.js",
              "js/tiny_mce/config.js",)


admin.site.register(Producto, ProductoAdmin)

