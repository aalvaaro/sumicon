from django.db import models
from autoslug import AutoSlugField
from cloudinary.models import CloudinaryField

CATEGORIAS = (
    ("clips", "Clips"),
    ("codos", "Codos"),
    ("conectores", "Conectores"),
    ("niples", "Niples"),
    ("postes", "Postes"),
    ("soportes", "Soportes"),
    ("tapones", "Tapones"),
)

class Producto(models.Model):
    nombre = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from = "nombre", always_update = True,
                         unique_with = "id")
    categoria = models.CharField(max_length=30, null=True, blank=True,
                                 choices=CATEGORIAS)
    imagen = CloudinaryField(null=True, blank=True)
    ficha_tecnica = CloudinaryField(null=True, blank=True)
    clave = models.CharField(max_length=30, null=True, blank=True)
    codigo_barras = models.CharField(max_length=100, null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)
    cantidad = models.IntegerField(null=True, blank=True)
    costo = models.DecimalField(max_digits=10, decimal_places=2, null=True,
                                blank=True)
    precio = models.DecimalField(max_digits=10, decimal_places=2, null=True,
                                 blank=True)

    def __unicode__(self):
        return self.nombre



