from django.shortcuts import render_to_response
from django.views.generic import ListView, DetailView
from .models import Producto
# from paypal.standard.forms import PayPalPaymentsForm
from django.core.urlresolvers import reverse

CATEGORIAS = {
    "Clips",
    "Codos",
    "Conectores",
    "Niples",
    "Postes",
    "Soportes",
    "Tapones"
}

class CatalogoList(ListView):
    model = Producto
    template_name = "productos.html"
    context_object_name = "productos"

    def get_context_data(self, **kwargs):
        context = super(CatalogoList, self).get_context_data(**kwargs)
        context["categorias"] = CATEGORIAS
        return context

class CatalogoDetail(DetailView):
    model = Producto
    template_name = "detalles.html"
    context_object_name = "producto"

    def get_context_data(self, **kwargs):
        context = super(CatalogoDetail, self).get_context_data(**kwargs)
	context["categorias"] = CATEGORIAS
	return context

def home(request):
    productos = Producto.objects.all()[:8]
    context = {"productos": productos}
    return render_to_response("index.html", context)

def categoria(request, categoria):
    productos = Producto.objects.filter(categoria=categoria)
    context = {"productos": productos, "categorias": CATEGORIAS}
    return render_to_response("productos.html", context)


# def payment(request):

#     # What you want the button to do.
#     paypal_dict = {
#         "business": "o0.aalvaaro.0o@gmail.com",
#         "amount": "10000000.00",
#         "item_name": "name of the item",
#         "invoice": "unique-invoice-id",
#         "notify_url": "https://www.example.com" + reverse('paypal-ipn'),
#         "return_url": "https://www.example.com/your-return-location/",
#         "cancel_return": "https://www.example.com/your-cancel-location/",

#     }

#     # Create the instance.
#     form = PayPalPaymentsForm(initial=paypal_dict)
#     context = {"form": form}
#     return render_to_response("cart.html", context)




